CREATE DATABASE IF NOT EXISTS Colegio;
USE Colegio;

CREATE TABLE Estudiantes (
    ID_Estudiante INT AUTO_INCREMENT PRIMARY KEY,
    Nombre VARCHAR(50),
    Apellido VARCHAR(50),
    Fecha_Nacimiento DATE,
    Dirección VARCHAR(100),
    Teléfono VARCHAR(15),
    Grado_Actual VARCHAR(10)
);

CREATE TABLE Profesores (
    ID_Profesor INT AUTO_INCREMENT PRIMARY KEY,
    Nombre VARCHAR(50),
    Apellido VARCHAR(50),
    Especialidad VARCHAR(50),
    Teléfono VARCHAR(15),
    Email VARCHAR(100),
    Fecha_Contratación DATE
);

CREATE TABLE Cursos (
    ID_Curso INT AUTO_INCREMENT PRIMARY KEY,
    Nombre_Curso VARCHAR(100),
    Descripcion TEXT,
    ID_Profesor INT,
    Horario VARCHAR(50),
    Aula VARCHAR(20),
    Grado_Destinatario VARCHAR(10),
    FOREIGN KEY (ID_Profesor) REFERENCES Profesores(ID_Profesor)
);

INSERT INTO Estudiantes (ID_Estudiante, Nombre, Apellido, Fecha_nacimiento, Dirección, Teléfono, Grado_actual)
VALUES
(1, 'Juan', 'García', '2005-05-10', 'Calle Principal 123', '555-1234', '10° grado'),
(2, 'María', 'López', '2006-09-15', 'Avenida Central 456', '555-5678', '9° grado'),
(3, 'Carlos', 'Martínez', '2004-03-20', 'Calle Secundaria 789', '555-9012', '11° grado');

INSERT INTO Profesores (ID_Profesor, Nombre, Apellido, Especialidad, Teléfono, Email, Fecha_contratación)
VALUES
(1, 'Ana', 'Rodríguez', 'Matemáticas', '555-1111', 'ana@example.com', '2010-08-20'),
(2, 'Pedro', 'Sánchez', 'Historia', '555-2222', 'pedro@example.com', '2015-04-10'),
(3, 'Laura', 'Gómez', 'Ciencias', '555-3333', 'laura@example.com', '2018-01-15');

select*from Cursos;
INSERT INTO Cursos (ID_Curso, Nombre_curso, Descripción, ID_Profesor, Horario, Aula, Grado_destinatario)
VALUES
(1, 'Matemáticas I', 'Curso introductorio de matemáticas', 1, 'Lunes y Miércoles 10:00 - 11:30', 'Aula 101', '10° grado'),
(2, 'Historia Mundial', 'Recorrido por la historia mundial', 2, 'Martes y Jueves 09:00 - 10:30', 'Aula 202', '9° grado'),
(3, 'Biología Avanzada', 'Estudio detallado de la biología celular', 3, 'Lunes y Miércoles 14:00 - 15:30', 'Laboratorio 301', '11° grado');