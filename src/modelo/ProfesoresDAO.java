package modelo;

import java.sql.SQLException;
import java.util.ArrayList;

public interface ProfesoresDAO {
    ArrayList<Profesores> select_all() throws SQLException;
    Cursos select_by_id(int ID_Profesor);
    double select_min_funcion();
    boolean insert_profesores(Profesores profesores);
    boolean delete_profesores_by_id(int id);
    boolean update_quastity_by_id(int ID_Profesor, String Nombre);
    boolean chek_id();
}
