package modelo;

public class Profesores {
    private  int ID_Profesor;
    private  String Nombre;
    private  String Apellido;
    private  String Especialidad;
    private  String Teléfono;
    private  String Email;
    private  String Fecha_Contratación;

    public Profesores(int ID_Profesor, String nombre, String apellido, String especialidad, String telefono, String email, String fecha_Contratación) {
        this.ID_Profesor = ID_Profesor;
        Nombre = nombre;
        Apellido = apellido;
        Especialidad = especialidad;
        Teléfono = telefono;
        Email = email;
        Fecha_Contratación = fecha_Contratación;
    }

    public  int getID_Profesor() {
        return ID_Profesor;
    }

    public void setID_Profesor(int ID_Profesor) {
        this.ID_Profesor = ID_Profesor;
    }

    public  String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public  String getApellido() {
        return Apellido;
    }

    public void setApellido(String apellido) {
        Apellido = apellido;
    }

    public  String getEspecialidad() {
        return Especialidad;
    }

    public void setEspecialidad(String especialidad) {
        Especialidad = especialidad;
    }

    public  String getTelefono() {
        return Teléfono;
    }

    public void setTelefono(String teléfono) {
        Teléfono = teléfono;
    }

    public  String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public  String getFecha_Contratación() {
        return Fecha_Contratación;
    }

    public void setFecha_Contratacion(String fecha_Contratación) {
        Fecha_Contratación = fecha_Contratación;
    }

    @Override
    public String toString() {
        return "Profesores{" +
                "ID_Profesor=" + ID_Profesor +
                ", Nombre='" + Nombre + '\'' +
                ", Apellido='" + Apellido + '\'' +
                ", Especialidad='" + Especialidad + '\'' +
                ", Teléfono='" + Teléfono + '\'' +
                ", Email='" + Email + '\'' +
                ", Fecha_Contratación='" + Fecha_Contratación + '\'' +
                '}';
    }
}
