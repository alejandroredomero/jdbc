package modelo;

import java.sql.SQLException;
import java.util.ArrayList;

public interface EstudiantesDAO {
    ArrayList<Estudiantes> select_all() throws SQLException;
    Estudiantes select_by_id(int ID_Estudiante);
    double select_min_funcion();
    boolean insert_estudiante(Estudiantes estudiantes);
    boolean delete_estudiante_by_id(int id);
    boolean update_quastity_by_id(int ID_Estudiante, String Nombre);
    boolean chek_id();

}
