package modelo;

public class Cursos {
    private int ID_Curso;
    private  String Nombre_Curso;
    private String Descripción;
    private  int ID_Profesor;
    private  String Horario;
    private  String Aula;
    private  String Grado_Destinatario;

    public Cursos(int ID_Curso, String nombre_Curso, String descripción, int ID_Profesor, String horario, String aula, String grado_Destinatario) {
        this.ID_Curso = ID_Curso;
        Nombre_Curso = nombre_Curso;
        Descripción = descripción;
        this.ID_Profesor = ID_Profesor;
        Horario = horario;
        Aula = aula;
        Grado_Destinatario = grado_Destinatario;
    }

    public Cursos(int idEstudiante, String nombre, String apellido, String fechaNacimiento, String direccion, String telefono, String gradoActual) {
    }

    public  int getID_Curso() {
        return ID_Curso;
    }

    public void setID_Curso(int ID_Curso) {
        this.ID_Curso = ID_Curso;
    }

    public  String getNombre_Curso() {
        return Nombre_Curso;
    }

    public void setNombre_Curso(String nombre_Curso) {
        Nombre_Curso = nombre_Curso;
    }

    public  String getDescripcion() {
        return Descripción;
    }

    public void setDescripcion(String descripción) {
        Descripción = descripción;
    }

    public  int getID_Profesor() {
        return ID_Profesor;
    }

    public void setID_Profesor(int ID_Profesor) {
        this.ID_Profesor = ID_Profesor;
    }

    public  String getHorario() {
        return Horario;
    }

    public void setHorario(String horario) {
        Horario = horario;
    }

    public  String getAula() {
        return Aula;
    }

    public void setAula(String aula) {
        Aula = aula;
    }

    public  String getGrado_Destinatario() {
        return Grado_Destinatario;
    }

    public void setGrado_Destinatario(String grado_Destinatario) {
        Grado_Destinatario = grado_Destinatario;
    }

    @Override
    public String toString() {
        return "Cursos{" +
                "ID_Curso=" + ID_Curso +
                ", Nombre_Curso='" + Nombre_Curso + '\'' +
                ", Descripcion='" + Descripción + '\'' +
                ", ID_Profesor=" + ID_Profesor +
                ", Horario='" + Horario + '\'' +
                ", Aula='" + Aula + '\'' +
                ", Grado_Destinatario='" + Grado_Destinatario + '\'' +
                '}';
    }
}
