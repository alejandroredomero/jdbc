package modelo;

public class Estudiantes {
    private  int ID_Estudiante;
    private  String Nombre;
    private  String Apellido;
    private  String Fecha_Nacimiento;
    private  String Dirección;
    private  String Teléfono;
    private  String Grado_Actual;

    public Estudiantes(int ID_Estudiante, String nombre, String apellido, String fecha_Nacimiento, String direccion, String telefono, String grado_Actual) {
        this.ID_Estudiante = ID_Estudiante;
        Nombre = nombre;
        Apellido = apellido;
        Fecha_Nacimiento = fecha_Nacimiento;
        Dirección = direccion;
        Teléfono = telefono;
        Grado_Actual = grado_Actual;
    }

    public  int getID_Estudiante() {
        return ID_Estudiante;
    }

    public void setID_Estudiante(int ID_Estudiante) {
        this.ID_Estudiante = ID_Estudiante;
    }

    public  String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public  String getApellido() {
        return Apellido;
    }

    public void setApellido(String apellido) {
        Apellido = apellido;
    }

    public  String getFecha_Nacimiento() {
        return Fecha_Nacimiento;
    }

    public void setFecha_Nacimiento(String fecha_Nacimiento) {
        Fecha_Nacimiento = fecha_Nacimiento;
    }

    public  String getDireccion() {
        return Dirección;
    }

    public void setDireccion(String dirección) {
        Dirección = dirección;
    }

    public  String getTelefono() {
        return Teléfono;
    }

    public void setTelefono(String teléfono) {
        Teléfono = teléfono;
    }

    public  String getGrado_Actual() {
        return Grado_Actual;
    }

    public void setGrado_Actual(String grado_Actual) {
        Grado_Actual = grado_Actual;
    }

    @Override
    public String toString() {
        return "Estudiantes{" +
                "ID_Estudiante=" + ID_Estudiante +
                ", Nombre='" + Nombre + '\'' +
                ", Apellido='" + Apellido + '\'' +
                ", Fecha_Nacimiento='" + Fecha_Nacimiento + '\'' +
                ", Dirección='" + Dirección + '\'' +
                ", Teléfono='" + Teléfono + '\'' +
                ", Grado_Actual='" + Grado_Actual + '\'' +
                '}';
    }
}
