package modelo;

import java.sql.SQLException;
import java.util.ArrayList;

public interface CursosDAO {
    ArrayList<Cursos> select_all() throws SQLException;
    Cursos select_by_id(int ID_Curso);
    double select_min_funcion();
    boolean insert_cursos(Cursos cursos);
    boolean delete_cursos_by_id(int id);
    boolean update_quastity_by_id_Curso(int ID_Curso, String Nombre_Curso);
    boolean chek_id();

}
