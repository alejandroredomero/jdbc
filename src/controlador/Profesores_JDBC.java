package controlador;

import modelo.Cursos;
import modelo.Estudiantes;
import modelo.Profesores;
import modelo.ProfesoresDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Profesores_JDBC implements ProfesoresDAO {

    public final String SELECT_ALL = "SELECT * FROM Profesores";
    public final String SELECT_BY_ID = "SELECT * FROM Profesores WHERE id = ?";
    public final String SELECT_MIN_FECHA_CONTRATACION = "SELECT min(Fecha_Contratación) FROM Profesores";
    public final String INSERT_Profesores = "SINSERT Profesores VALUES(?,?,?,?)";
    public final String DELETE_PROFESORES_BY_ID = "DELETE FROM Profesores WHERE ID_Profesor = ?";
    public final String UPDATE_QUASTITY_BY_ID = "DELETE FROM Profesores WHERE id = ? ?";
    public final String CHEK_ID = "SELECT ID_Profesor FROM Profesores WHERE ID_Profesor";


    Connection con;
    public Profesores_JDBC(Connection con){
        this.con=con;
    }


    @Override
    public ArrayList<Profesores> select_all() throws SQLException {
        ArrayList<Profesores> lista_user = new ArrayList<>();
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_ALL);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()){
                int ID_Profesor = rs.getInt("ID_Profesor");
                String Nombre = rs.getString("Nombre");
                String Apellido = rs.getString("Apellido");
                String Especialidad = rs.getString("Especialidad");
                String Teléfono = rs.getString("Teléfono");
                String Email = rs.getString("Email");
                String Fecha_Contratación = rs.getString("Fecha_Contratación");

                Profesores u1 = new Profesores(ID_Profesor, Nombre, Apellido, Especialidad, Teléfono, Email, Fecha_Contratación);
                lista_user.add(u1);

            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return lista_user;
    }

    @Override
    public Cursos select_by_id(int ID_Profesor) {
        Cursos u1 = null;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_BY_ID);
            stmt.setInt(1, ID_Profesor);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                ID_Profesor = rs.getInt("ID_Profesor");
                String Nombre = rs.getString("Nombre");
                String Apellido = rs.getString("Apellido");
                String Especialidad = rs.getString("Especialidad");
                String Teléfono = rs.getString("Telefono");
                String Email = rs.getString("Email");
                String Fecha_Contratación = rs.getString("Fecha_Contratación");


                u1 = new Cursos(ID_Profesor, Nombre, Apellido, Especialidad, Teléfono, Email, Fecha_Contratación);

            } else {
                u1 = null;
            }
            return null;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public double select_min_funcion() {
        double salario = 0;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_MIN_FECHA_CONTRATACION);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                salario = rs.getDouble("Fecha_Contratación");
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salario;
    }

    @Override
    public boolean insert_profesores(Profesores profesores) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(INSERT_Profesores);
            stmt.setInt(1, profesores.getID_Profesor());
            stmt.setString(2, profesores.getNombre());
            stmt.setString(3, profesores.getApellido());
            stmt.setString(4, profesores.getEspecialidad());
            stmt.setString(5, profesores.getTelefono());
            stmt.setString(6, profesores.getEmail());
            stmt.setString(7, profesores.getFecha_Contratación());


            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salida;
    }

    @Override
    public boolean delete_profesores_by_id(int ID_Profesores) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(DELETE_PROFESORES_BY_ID);
            stmt.setInt(1, ID_Profesores);

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salida;
    }

    @Override
    public boolean update_quastity_by_id(int ID_Profesor, String Nombre) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(UPDATE_QUASTITY_BY_ID);
            stmt.setDouble(1, ID_Profesor);
            stmt.setString(2, Nombre);


            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return salida;
    }

    @Override
    public boolean chek_id() {
        return false;
    }


}
