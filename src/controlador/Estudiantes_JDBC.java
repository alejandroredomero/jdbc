package controlador;

import modelo.Cursos;
import modelo.Estudiantes;
import modelo.EstudiantesDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Estudiantes_JDBC implements EstudiantesDAO {
    public final String SELECT_ALL = "SELECT * FROM Estudiantes";
    public final String SELECT_BY_ID = "SELECT * FROM Estudiantes WHERE id = ?";
    public final String SELECT_MIN_FECHA_NACIMIENTO = "SELECT min(Fecha_Nacimiento) FROM Estudiantes";
    public final String INSERT_ESTUDIANTES = "SINSERT Estudiantes VALUES(?,?,?,?)";
    public final String DELETE_ESTUDIANTES_BY_ID = "DELETE FROM Estudiantes WHERE ID_Estudiantes = ?";
    public final String UPDATE_QUASTITY_BY_ID = "DELETE FROM Estudiantes WHERE id = ? ?";
    public final String CHEK_ID = "SELECT ID_Estudiantes FROM Estudiantes WHERE ID_Estudiantes";

    Connection con;
    public Estudiantes_JDBC(Connection con){
        this.con=con;
    }


    @Override
    public ArrayList<Estudiantes> select_all() {
        ArrayList<Estudiantes> lista_user = new ArrayList<>();
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_ALL);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()){
                int ID_Estudiante = rs.getInt("ID_Estudiante");
                String Nombre = rs.getString("Nombre");
                String Apellido = rs.getString("Apellido");
                String Fecha_Nacimiento = rs.getString("Fecha_Nacimiento");
                String Dirección = rs.getString("Dirección");
                String Teléfono = rs.getString("Teléfono");
                String Grado_Actual = rs.getString("Grado_Actual");

                Estudiantes u1 = new Estudiantes(ID_Estudiante, Nombre, Apellido, Fecha_Nacimiento, Dirección, Teléfono, Grado_Actual);
                lista_user.add(u1);

            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return lista_user;
    }

    @Override
    public Estudiantes select_by_id(int ID_Estudiante) {
        Cursos u1 = null;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_BY_ID);
            stmt.setInt(1, ID_Estudiante);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                ID_Estudiante = rs.getInt("ID_Estudiante");
                String Nombre = rs.getString("Nombre");
                String Apellido = rs.getString("Apellido");
                String Fecha_Nacimiento = rs.getString("Fecha_Nacimiento");
                String Dirección = rs.getString("Dirección");
                String Teléfono = rs.getString("Teléfono");
                String Grado_Actual = rs.getString("Grado_Actual");


                u1 = new Cursos(ID_Estudiante, Nombre, Apellido, Fecha_Nacimiento, Dirección, Teléfono, Grado_Actual);

            } else {
                u1 = null;
            }
            return null;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public double select_min_funcion() {
        double salario = 0;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_MIN_FECHA_NACIMIENTO);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                salario = rs.getDouble("ID_Estudiantes");
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salario;
    }

    @Override
    public boolean insert_estudiante(Estudiantes estudiantes) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(INSERT_ESTUDIANTES);
            stmt.setInt(1, estudiantes.getID_Estudiante());
            stmt.setString(2, estudiantes.getNombre());
            stmt.setString(3, estudiantes.getApellido());
            stmt.setString(4, estudiantes.getFecha_Nacimiento());
            stmt.setString(2, estudiantes.getDireccion());
            stmt.setString(3, estudiantes.getTelefono());
            stmt.setString(2, estudiantes.getGrado_Actual());


            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salida;
    }

    @Override
    public boolean delete_estudiante_by_id(int ID_Estudiantes) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(DELETE_ESTUDIANTES_BY_ID);
            stmt.setInt(1, ID_Estudiantes);

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salida;
    }

    @Override
    public boolean update_quastity_by_id(int ID_Estudiante, String Nombre) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(UPDATE_QUASTITY_BY_ID);
            stmt.setDouble(1, ID_Estudiante);
            stmt.setString(2, Nombre);


            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return salida;
    }

    @Override
    public boolean chek_id() {
        return false;
    }
}
