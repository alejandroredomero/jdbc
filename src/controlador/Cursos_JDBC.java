package controlador;

import modelo.Cursos;
import modelo.CursosDAO;
import modelo.Estudiantes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Cursos_JDBC implements CursosDAO {
    public final String SELECT_ALL = "SELECT * FROM Cursos";
    public final String SELECT_BY_ID = "SELECT * FROM Cursos WHERE ID_Curso = ?";
    public final String SELECT_MIN_ID_Profesor = "SELECT min(ID_Profesor) FROM Cursos";
    public final String INSERT_USER = "SINSERT Cursos VALUES(?,?,?,?)";
    public final String DELETE_CURSOS_BY_ID = "DELETE FROM Cursos WHERE ID_Curso = ?";
    public final String UPDATE_QUASTITY_BY_ID = "DELETE FROM Cursos WHERE id = ? ?";
    public final String CHEK_ID = "SELECT ID_Curso FROM Cursos WHERE ID_Curso";

    Connection con;
    public Cursos_JDBC(Connection con){
        this.con=con;
    }

    @Override
    public ArrayList<Cursos> select_all() throws SQLException {
        ArrayList<Cursos> lista_user = new ArrayList<>();
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_ALL);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()){
                int ID_Curso = rs.getInt("ID_Curso");
                String Nombre_Curso = rs.getString("Nombre_Curso");
                String Descripción = rs.getString("Descripción");
                int ID_Profesor = rs.getInt("ID_Profesor");
                String Horario = rs.getString("Horario");
                String Aula = rs.getString("Aula");
                String Grado_Destinatario = rs.getString("Grado_Destinatario");

                Cursos u1 = new Cursos(ID_Curso, Nombre_Curso, Descripción, ID_Profesor, Horario, Aula, Grado_Destinatario);
                lista_user.add(u1);

            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return lista_user;
    }

    @Override
    public Cursos select_by_id(int ID_Curso) {
        Cursos u1 = null;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_BY_ID);
            stmt.setInt(1, ID_Curso);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                ID_Curso = rs.getInt("ID_Curso");
                String Nombre_Curso = rs.getString("Nombre_Curso");
                String Descripcion = rs.getString("Descripcion");
                int ID_Profesor = rs.getInt("ID_Profesor");
                String Horario = rs.getString("Horario");
                String Aula = rs.getString("Aula");
                String Grado_Destinatario = rs.getString("Grado_Destinatario");


                u1 = new Cursos(ID_Curso, Nombre_Curso, Descripcion, ID_Profesor, Horario, Aula, Grado_Destinatario);

            }else{
                u1 = null;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return u1;
    }

    @Override
    public double select_min_funcion() {
        double salario = 0;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_MIN_ID_Profesor);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                salario = rs.getDouble("ID_Profesor");
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salario;
    }

    @Override
    public boolean insert_cursos(Cursos cursos) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(INSERT_USER);
            stmt.setInt(1, cursos.getID_Curso());
            stmt.setString(2, cursos.getNombre_Curso());
            stmt.setString(3, cursos.getDescripcion());
            stmt.setInt(4, cursos.getID_Profesor());
            stmt.setString(2, cursos.getHorario());
            stmt.setString(3, cursos.getAula());
            stmt.setString(2, cursos.getGrado_Destinatario());


            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return salida;
    }

    @Override
    public boolean delete_cursos_by_id(int ID_Curso) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(DELETE_CURSOS_BY_ID);
            stmt.setInt(1, ID_Curso);

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salida;
    }

    @Override
    public boolean update_quastity_by_id_Curso(int ID_Curso, String Nombre_Curso) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(UPDATE_QUASTITY_BY_ID);
            stmt.setDouble(1, ID_Curso);
            stmt.setString(2, Nombre_Curso);


            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


        return salida;

    }

    @Override
    public boolean chek_id() {
        return false;
    }


}

