package vista;

import controlador.Cursos_JDBC;
import controlador.Estudiantes_JDBC;
import controlador.Profesores_JDBC;
import modelo.Cursos;
import modelo.Estudiantes;
import modelo.Profesores;
import utilidades.Conexion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Conexion c = new Conexion();
        Connection conn =  c.getConnection();
        PreparedStatement stmt = null;



        try {

            System.out.println("Connecting to database...");


            Scanner sc = new Scanner(System.in);
            boolean sigue = true;
            while(sigue) {
                System.out.println("eligue la tabla que quieras usar \n" +
                        "1- Cursos\n" +
                        "2- Estudiantes\n" +
                        "3- Profesores\n" +
                        "4- Salir");
                int num = sc.nextInt();
                switch (num) {
                        case 1:
                            System.out.println("Has elegido la tabla Cursos, que quieres hacer con ella?\n" +
                                    "1- select_all\n" +
                                    "2- select_by_id\n" +
                                    "3- select_min_fundacion\n" +
                                    "4- insert_equipo\n" +
                                    "5- delete_equipo_by_id\n" +
                                    "6- update_entrenador_by_id\n" +
                                    "7- update_all_by_id_equipo\n" +
                                    "8- Salir");
                            int elec = sc.nextInt();
                            switch(elec){
                                case 1:
                                    Cursos_JDBC controller = new Cursos_JDBC(conn);
                                    ArrayList<Cursos> lista_curso = controller.select_all();
                                    for (int i = 0; i < lista_curso.size(); i++) {
                                        System.out.println(lista_curso.get(i));
                                    }
                                    break;
                            }
                            break;

                        case 2:
                            System.out.println("Has elegido la tabla Estudiantes, que quieres hacer con ella?\n" +
                                    "1- select_all\n" +
                                    "2- select_by_id\n" +
                                    "3- select_min_fundacion\n" +
                                    "4- insert_equipo\n" +
                                    "5- delete_equipo_by_id\n" +
                                    "6- update_entrenador_by_id\n" +
                                    "7- update_all_by_id_equipo\n" +
                                    "8- Salir");

                            int elecc = sc.nextInt();
                            switch(elecc){
                                case 1:
                                    Estudiantes_JDBC controller1 = new Estudiantes_JDBC(conn);
                                    ArrayList<Estudiantes> lista_estudiantes = controller1.select_all();
                                    for (Estudiantes e : lista_estudiantes) {
                                        System.out.println(e);
                                    }
                                    break;
                            }
                            break;

                        case 3:
                            System.out.println("Has elegido la tabla Estudiantes, que quieres hacer con ella?\n" +
                                    "1- select_all\n" +
                                    "2- select_by_id\n" +
                                    "3- select_min_fundacion\n" +
                                    "4- insert_equipo\n" +
                                    "5- delete_equipo_by_id\n" +
                                    "6- update_entrenador_by_id\n" +
                                    "7- update_all_by_id_equipo\n" +
                                    "8- Salir");

                            int el = sc.nextInt();
                            switch(el){
                                case 1:
                                    Profesores_JDBC controller2 = new Profesores_JDBC(conn);
                                    ArrayList<Profesores> lista_profesores = controller2.select_all();
                                    for (Profesores p : lista_profesores) {
                                        System.out.println(p);
                                    }
                                    break;
                            }
                            break;

                    case 4:
                        sigue = false;
                        break;
                    }
                }



        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }// nothing we can do
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
        System.out.println("Goodbye!");
    }//end main
}


